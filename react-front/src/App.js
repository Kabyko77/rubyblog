import { useState } from 'react';
import './App.css';
import Auth from './components/Auth/Auth';
import AddPost from './components/Posts/AddPost';
import Posts from './components/Posts/Posts';
import Search from './components/Posts/Search';

function App() {


  const [posts,setPosts] = useState([])
  const [user, setUser] = useState(
    {
      username: localStorage.getItem("username"),
      user_id: localStorage.getItem("user_id"),
      token: localStorage.getItem("token"),
      role: localStorage.getItem("role")
    }
  )
  const setUserToLocalStarage = (RememberMe, value) => {
    if (RememberMe) {
      localStorage.setItem("username", value.username);
      localStorage.setItem("token", value.token);
      localStorage.setItem("user_id", value.user_id);
      localStorage.setItem("role", value.role);
    }
    setUser(value);
  }
  const logOut = () => {
    localStorage.setItem("username", "");
    localStorage.setItem("token", "");
    localStorage.setItem("user_id", "");
    localStorage.setItem("role", "");
    setUser({
      username: "",
      token: "",
      user_id: "",
      role: ""
    });
  }
  const ShowPosts = () => {
    for (let value in user) {
      if (!user[value])
        return false
    }
    return true;
  }

  return (
    <div className="container bg-light">
      { !ShowPosts() ?
        <>
          <Auth setUserToLocalStarage={setUserToLocalStarage}></Auth>
        </>
        :
        <>
        <div className="py-5">
          <button type="button  w-75" onClick={logOut} className="btn btn-secondary btn-lg btn-block">Log Out</button>
        </div>
        <AddPost posts={posts} setPosts={setPosts} user={user}></AddPost>
        <Search posts={posts} setPosts={setPosts} user={user}></Search>
        <Posts posts={posts} setPosts={setPosts} user={user}></Posts>

        </>

      }
    </div>
  );
}

export default App;
