import axios from 'axios';
import React, { useRef, useEffect, useState } from 'react';



export default function AddPost(props) {

    const FormRef = useRef(null);
    const [category, setCategory] = useState([]);
    useEffect(() => {
        axios.get("http://localhost:5000/post_categories",
            { headers: { 'Authorization': `bearer ${props.user.token}` } })
            .then(x => {
                setCategory([...x.data]);
            })
    }, [])

    const confirm = (ev)=>
    {   
        ev.preventDefault();
        let form = FormRef.current;

        console.log(form["Category"].selectedIndex);

        if (form["Title"].value.trim() === "" ||
            form["Body"].value.trim() === "" ||
            form["Tags"].value.trim() === "" ||
            form["Category"].selectedIndex === 0) { alert("empty field"); return; }

         axios.post("http://localhost:5000/posts", {
             name: form["Title"].value,
             text: form["Body"].value,
             user_id: props.user.user_id,
             tags: form["Tags"].value.split(" "),
             post_category_id: category[form["Category"].selectedIndex-1].id 
         }, { headers: { 'Authorization': `bearer ${props.user.token}` } }).then(x => {
            alert("Posted");  
                axios.get("http://localhost:5000/posts",
                    { headers: { 'Authorization': `bearer ${props.user.token}` } }
                ).then(x => { props.setPosts([...x.data]); });
            
         }).catch((err) => { if (err.message) alert("Something went wrong") });
    }

    return (
        
            <div className="div-input-fiald mb-5 bg-white border border-dark">
                <form className="w-75 mx-auto py-4" ref={FormRef}>
                    <h1 className="text-center">Create new post</h1>
                    <input type="text" name={"Title"} className="form-control mt-4" placeholder="Title" required />
                    <input type="text" name={"Body"} className="form-control mt-4" placeholder="Body" required />
                    <input type="text" name={"Tags"} className="form-control mt-4" placeholder="Tags" required />
                   
                    <div className="form-group">
                        <select name={"Category"} className="form-control mt-4" id="Select">
                            <option defaultValue>Select Category</option>
                            {category.length > 0 && 
                                <>
                                    {category.map(x=><option key={"categoryKey "+x.name} defaultValue>{x.name}</option>)}
                                </>
                            }
                        </select>
                    </div>
                    
                    <div className="mt-3 col-lg-12 d-flex justify-content-end">
                        <button type="submit" onClick={(ev) => confirm(ev)} className="btn btn-primary">Create</button>
                    </div>
                </form>
            </div>
     

    )
}