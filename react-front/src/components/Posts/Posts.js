import axios from 'axios';
import React, { useRef, useEffect } from 'react';
import "./Posts.css"


export default function Posts(props) {

    const PostRef = useRef(null);
    useEffect(
        () => {
            axios.get("http://localhost:5000/posts",
                { headers: { 'Authorization': `bearer ${props.user.token}` } }
            ).then(x => { console.log(x.data); props.setPosts([...x.data]); });
        },
        []
    )

    const getPosts = () => {
        axios.get("http://localhost:5000/posts",
            { headers: { 'Authorization': `bearer ${props.user.token}` } }
        ).then(x => { props.setPosts([...x.data]); });
    }

    const Like = (arrLikes, post_id) => {
        let temp = arrLikes.find(x => x.user_id == props.user.user_id)
        if (temp) {
            axios.delete(`http://localhost:5000/likes/${temp.id}`,
                { headers: { 'Authorization': `bearer ${props.user.token}` } })
                .then(x => {
                    alert(x.data.message); getPosts();
                })
        }
        else {
            axios.post(`http://localhost:5000/likes`, { post_id: post_id, user_id: props.user.user_id },
                { headers: { 'Authorization': `bearer ${props.user.token}` } })
                .then(x => {
                    alert(x.data.message); getPosts();
                });
        }

    };

    const addComment = (id) => {


        if (document.getElementById(`Post comment ${id}`).value.trim() == "") {
            alert("Empty commands fiald");
            return;
        }
        else {

            axios.post(`http://localhost:5000/comments`, {
                text: document.getElementById(`Post comment ${id}`).value,
                user_id: props.user.user_id,
                post_id: id
            },
                { headers: { 'Authorization': `bearer ${props.user.token}` } })
                .then(x => {
                    alert(x.data.message); getPosts();
                }).catch(p => getPosts());
        }

    }
    const getCreatedDate = (time) => {
        let temp = new Date(time.replace('T', ' ').replace('Z', ''))
        return `${temp.getDate()}/${temp.getMonth()}/${temp.getFullYear()}`
    }

    const deletePost =(val)=>
    {
        axios.delete(`http://localhost:5000/posts/${val}`,
            { headers: { 'Authorization': `bearer ${props.user.token}` } })
            .then(x => {
                alert(x.data.message); getPosts();
            }).catch(p => getPosts());
    }
    return (
        <>
            {
                props.posts.map(x =>

                    <div key={`post ${x.id}`} className="bg-white border border-dark mb-4" >

                        <div className="mb-3">
                            <h1 className="text-center text-primary">{x.name}</h1>
                        </div>
                        <div className="mb-3">
                            <h3 className="text-center text-muted">Category: {x.post_category.name}</h3>
                        </div>
                        <div className="mb-3">
                            <h3 className="text-center">Created By {x.user.username} <h6>{getCreatedDate(x.created_at)}</h6></h3>
                        </div>
                        <div className="mb-3 d-flex justify-content-center">
                            {(x.user.id == props.user.user_id || props.user.role == 2)
                                &&
                                <button onClick={(ev) => deletePost(x.id)} className="btn btn-danger">Delete</button>
                            }
                        </div>
                        <div className="mb-3 mx-5">
                            <p className="text-justify">{x.text}</p>
                        </div>

                        <div className="mb-3 ml-5 mr">
                            <i onClick={() => Like(x.likes, x.id)} className={`fas fa-heart  ${x.likes.find(x => x.user_id == props.user.user_id) ? "liked" : "notliked"}`}></i>
                            <p className="d-inline likesCount">{x.likes.length}</p>
                            <button className="btn  btn-primary mx-3 mb-2" type="button" data-toggle="collapse" data-target={`#comments post ${x.id}`} aria-expanded="false" aria-controls={`comments post ${x.id}`}>
                                See comments
                        </button>
                            {
                                x.post_tags.map(h => <h5 key={`tags ${h.id}`} className="ml-1 bg-info d-inline px-1">{h.name}</h5>)
                            }
                            <div className="input-group mb-3">
                                <input type="text" id={`Post comment ${x.id}`} className="form-control input" placeholder="comment post" />
                                <a className="btn  btn-primary ml-3 mr-4" onClick={(ev) => addComment(x.id)} type="button" >Add comment</a>
                            </div>
                        </div>

                        <div className="collapse mx-3" id={`comments post ${x.id}`}>
                            <div className="p-3">
                                {
                                    x.comments.map(y =>
                                        <div key={`comments ${y.id}`} className="border border-dark mb-2 p-3" >
                                            <h4>User_id : {y.user_id}</h4>
                                            <p>{y.text}</p>
                                        </div>)
                                }
                            </div>
                        </div>
                    </div >
                )
            }
        </>
    )


}