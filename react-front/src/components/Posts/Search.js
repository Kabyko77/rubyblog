import axios from 'axios';
import React, { useRef, useEffect, useState } from 'react';
import "./Posts.css"


export default function Search(props) {

    const findByRef = useRef(null);
    const [category, setCategory] = useState([]);
    const [tags, setTags] = useState([]);
    useEffect(() => {
        axios.get("http://localhost:5000/post_categories",
            { headers: { 'Authorization': `bearer ${props.user.token}` } })
            .then(x => {
                setCategory([...x.data]);

            }).then(axios.get("http://localhost:5000/post_tags",
                { headers: { 'Authorization': `bearer ${props.user.token}` } })
                .then(g => {
                    let temp = [...new Set(g.data.map(g => g.name))]
                    setTags(temp);
                }))
    }, [])

    const searchTitle = () => {
        let temp = "";
        temp = findByRef.current["Title"].value;
        axios.get("http://localhost:5000/searchTitle?name=" + temp, { headers: { 'Authorization': `bearer ${props.user.token}` } })
            .then(x => {
                props.setPosts([...x.data]);
            }).catch((err) => { if (err.message) alert("Error") });
    }

    const findByTags = () => {
        let temp = "";
        if (findByRef.current["Tags"].selectedIndex != 0)
            temp = tags[findByRef.current["Tags"].selectedIndex - 1];
        else return;
        console.log(temp);
        axios.get("http://localhost:5000/searchTags?name=" + temp, { headers: { 'Authorization': `bearer ${props.user.token}` } })
            .then(x => {
                props.setPosts([...x.data]);
            }).catch((err) => { if (err.message) alert("Error") });
    }

    const searchCategory = () => {
        let temp = "";
        if (findByRef.current["Category"].selectedIndex != 0)
            temp = category[findByRef.current["Category"].selectedIndex - 1].id;
        axios.get("http://localhost:5000/searchCategory?name=" + temp, { headers: { 'Authorization': `bearer ${props.user.token}` } })
            .then(x => {
                props.setPosts([...x.data]);
            }).catch((err) => { if (err.message) alert("Error") });
    }


    return (
        <div className="div-input-fiald bg-white  mb-5 border border-dark">
            <form className="w-75 mx-auto my-5 py-4" ref={findByRef}>
                <h1 className="text-center">Search Posts</h1>

                <div className="input-group mb-3">
                    <input type="text" name={"Title"} className="form-control " placeholder="Tittle" aria-label="Recipient's username" />
                    <a className="btn btn-primary ml-3" onClick={(ev) => searchTitle()} type="button" >Find by Tittle</a>
                </div>

                <div className="input-group mb-3">
                    <select name={"Tags"} className="form-control " id="Select">
                        <option defaultValue>Select Tags</option>
                        {tags.length > 0 &&
                            <>
                                {tags.map(x => <option key={"tags  " + x} >{x}</option>)}
                            </>
                        }
                    </select>
                    <a className="btn btn-primary ml-3" onClick={(ev) => findByTags()} type="button">Find by Tags</a>
                </div>

                <div className="input-group mb-3">
                    <select name={"Category"} className="form-control " aria-label="Recipient's username" id="Select">
                        <option defaultValue>Select Category</option>
                        {category.length > 0 &&
                            <>
                                {category.map(x => <option key={"category " + x.name} >{x.name}</option>)}
                            </>
                        }
                    </select>
                    <a className="btn btn-primary ml-3" onClick={(ev) => searchCategory()} type="button">Find by Category</a>
                </div>
            </form>
        </div>


    )
}