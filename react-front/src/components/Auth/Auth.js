import axios from 'axios';
import React, { useRef } from 'react';


export default function Auth(props) {

    const FormRef = useRef(null);
    const signin = (ev,Type) => {
        ev.preventDefault();
        let formCurrent = FormRef.current;
        if (formCurrent["Username"].value.trim() === "" || formCurrent["Password"].value.trim() === "") {
            alert("Wrong input data");
            return;
        }

        axios.post("http://localhost:5000/"+Type,
            {
                username: formCurrent["Username"].value,
                password: formCurrent["Password"].value
            }).then(x => {
                if (x.data.error)
                    alert("something went wrong");
                else {
                    props.setUserToLocalStarage(formCurrent["RememeberMe"].checked,
                    {
                        username: x.data.user.username,
                        user_id: x.data.user.id,
                        token: x.data.token,
                        role: x.data.user.user_role_id
                    });
                }
            })
    }

    return (
        <div className="py-5">
            <form ref={FormRef} className="w-75 m-auto bg-white p-3 border border-dark">
                <div className="form-group">
                    <label htmlFor="InputUsername">Email address</label>
                    <input  name="Username" type="text" className="form-control" id="InputUsername" placeholder="Enter username" />
                </div>
                <div className="form-group">
                    <label  htmlFor="InputPassword1">Password</label>
                    <input name="Password" type="password" className="form-control" id="InputPassword1" placeholder="Password" />
                </div>
                <div className="form-check mb-2">
                    <input name="RememeberMe" type="checkbox" className="form-check-input" id="Check" />
                    <label className="form-check-label" htmlFor="Check">Remember me</label>
                </div>
                <button type="submit" onClick={(ev)=>signin(ev,"/login")} className="btn btn-primary mr-3">Sign in</button>
                <button type="submit" onClick={(ev)=>signin(ev,"/users")} className="btn btn-primary">Sign up</button>
            </form>
        </div>
    )

}

