user_role = UserRole.create(name: "test role")
user_role = UserRole.create(name: "andmin")

user = User.create(username: "Yurii", password: "Yurii", user_role_id: 1)
user = User.create(username: "YuriiDanger", password: "YuriiDanger", user_role_id: 2)

post_category = PostCategory.create(name: "test category")
post_category = PostCategory.create(name: "test category2")
post = Post.create(name: "test post",
                   text: "test body",
                   post_category_id: post_category.id,
                   user_id: 1)

post_tag = PostTag.create(name: "name", post_id: 1)
post_tag = PostTag.create(name: "teg2", post_id: 1)

like = Like.create(post_id: 1, user_id: 1)

comment = Comment.create(text: "text", user_id: 1, post_id: 1)
