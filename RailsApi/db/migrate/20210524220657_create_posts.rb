class CreatePosts < ActiveRecord::Migration[6.1]
  def change
    create_table :posts do |t|
      t.references :post_category, null: false, foreign_key: true
      t.string :name
      t.string :text
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end
