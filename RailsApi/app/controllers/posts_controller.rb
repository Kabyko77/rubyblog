class PostsController < ApplicationController
  before_action :set_post, only: [:show, :update, :destroy]

  # GET /posts
  def index
    @posts = Post.all
    render json: @posts
  end

  # GET /posts/1
  def show
    render json: @post
  end

  def searchTitle
    @posts = Post.where("name like ?", "%#{params[:name]}%")
    render json: @posts
  end

  def findByTags
    tmp = PostTag.where(name:params[:name]).pluck(:post_id)
    @posts = Post.where(id:tmp)
    render json: @posts
  end

  def searchCategory
    @posts = Post.where(post_category_id: params[:name])
    render json: @posts
  end


  # POST /posts
  def create
    @post = Post.new(
      name:params[:name], 
      text:params[:body], 
      user_id:params[:user_id], 
      post_category_id:params[:post_category_id]
    )

    
    if @post.save
      render json: @post, status: :created
    else
      render json: @post.errors, status: :unprocessable_entity
    end

    params[:tags].each do |tagname|
      PostTag.create(name: tagname, post_id:@post.id)
    end
  end

  # PATCH/PUT /posts/1
  def update
    if @post.update(post_params)
      render json: @post
    else
      render json: @post.errors, status: :unprocessable_entity
    end
  end

  # DELETE /posts/1
  def destroy
    @post.destroy
    render json: {message:"deleted"}
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def post_params
      params.require(:post).permit(:name, :text, :user_id, :post_category_id)
    end
end
