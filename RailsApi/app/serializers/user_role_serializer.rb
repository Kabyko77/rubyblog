class UserRoleSerializer < ActiveModel::Serializer
  attributes :id, :name
end
