class LikeSerializer < ActiveModel::Serializer
  attributes :id, :user_id
  has_one :user
  has_one :post
end
