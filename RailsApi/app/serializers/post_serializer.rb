class PostSerializer < ActiveModel::Serializer
  attributes :id, :name, :text, :created_at
  has_one :post_category
  has_one :user

  has_many :post_tags
  has_many :comments
  has_many :likes
end
