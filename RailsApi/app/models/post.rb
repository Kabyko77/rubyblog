class Post < ApplicationRecord
  belongs_to :user
  belongs_to :post_category

  has_many :post_tags, :dependent => :destroy
  has_many :comments, :dependent => :destroy
  has_many :likes, :dependent => :destroy
end
