Rails.application.routes.draw do
  get "searchCategory", to: "posts#searchCategory"
  get "searchTitle", to: "posts#searchTitle"
  get "searchTags", to: "posts#findByTags"
  resources :comments
  resources :categories
  resources :likes
  resources :posts
  resources :post_tags
  resources :post_categories
  resource :users, only: [:create]
  post "/login", to: "users#login"
  resources :user_roles
end
