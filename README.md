# Rails
# Done:
* getting started
* authentication by different users  
* admin can delete user posts (admin username: YuriiDanger, password: YuriiDanger)   
* filtering by categories  
* filtering by tags  
* search by title of a post   
* JSON API + React client + JWT 
* likes
* deployed on heroku (React app: https://mighty-reaches-53846.herokuapp.com/  Back-end JSON-example: https://enigmatic-hamlet-63119.herokuapp.com/posts)
